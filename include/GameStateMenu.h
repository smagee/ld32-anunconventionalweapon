#ifndef GAMESTATEMENU_H
#define GAMESTATEMENU_H

#include <SFML/Graphics.hpp>

#include "GameState.h"

class GameStateMenu : public GameState
{
	public:
		GameStateMenu(Game* game);
		virtual ~GameStateMenu();

		void checkState();
		void pollEvent();
		void checkInput();
		void updateAI();
		void updateObjects();
		void checkCollision();
		void updateWindow();
	protected:
	private:
		enum MenuAction { Nothing, Play, Exit };
		struct MenuButton
		{
			sf::IntRect rect;
			MenuAction action;
		};

		void startGame();
		void exitGame();
		MenuAction handleClick(sf::Vector2i pos);

		std::vector<MenuButton> buttons_;
		sf::Sprite menu_;
		sf::Texture texture_;
};

#endif // GAMESTATEMENU_H
