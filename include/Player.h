#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

#include <SFML/Graphics.hpp>

#include "Entity.h"
#include "Bullet.h"

class Player : public Entity, public sf::Drawable, public sf::Transformable
{
	public:
		Player(sf::Vector2f position);
		virtual ~Player();

		void moveBy(sf::Vector2f offset);
		void moveBy(int x, int y);

		void dodge();
		void collect();
		void shoot(sf::Vector2f direction);
		sf::Vector2f getDirection(sf::Vector2i mousePos);

		std::vector<Bullet*> getBullets();
		Bullet* getBullet(int index);
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		//int bulletNum_;
		std::vector<Bullet*> bullets_;

		sf::Sprite player_;
		sf::Sprite bullet_;
		sf::Texture texture_;
};

#endif // PLAYER_H
