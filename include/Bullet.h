#ifndef BULLET_H
#define BULLET_H

#include <SFML/Graphics.hpp>

class Bullet : public sf::Drawable, public sf::Transformable
{
	public:
		Bullet(sf::Sprite bullet, sf::Vector2f direction, sf::Vector2f pos);
		virtual ~Bullet();

		void move();
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::Vector2f direction_;

		sf::Sprite bullet_;
};

#endif // BULLET_H
