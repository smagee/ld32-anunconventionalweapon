#ifndef GAME_H
#define GAME_H

#include <stack>

#include <SFML/Graphics.hpp>

class GameState;

class Game
{
	public:
		Game();
		virtual ~Game();

		int startGame();

		void pushState(GameState* state);
		void popState();
		void changeState(GameState* state);

		sf::RenderWindow window_;
	protected:
	private:
		int initGame();
		void gameLoop();

		std::stack<GameState*> states_;

		GameState* currentState();
};

#endif // GAME_H
