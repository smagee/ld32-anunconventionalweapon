#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "Game.h"

class GameState
{
	public:
		virtual void checkState() = 0;
		virtual void pollEvent() = 0;
		virtual void checkInput() = 0;
		virtual void updateAI() = 0;
		virtual void updateObjects() = 0;
		virtual void checkCollision() = 0;
		virtual void updateWindow() = 0;

		Game* game_;
	protected:
	private:
};

#endif // GAMESTATE_H
