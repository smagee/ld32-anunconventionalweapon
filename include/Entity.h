#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

class Entity
{
	public:
		Entity();
		virtual ~Entity();

		float getMovementSpeed();
		int getHealth();
	protected:
		float movement_;
		int health_;
	private:
};

#endif // ENTITY_H
