#ifndef GAMESTATEEND_H
#define GAMESTATEEND_H

#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "GameStateMenu.h"
#include "GameStateGame.h"

class GameStateEnd : public GameState
{
	public:
		GameStateEnd(Game* game);
		virtual ~GameStateEnd();
	protected:
	private:
		void checkState();
		void pollEvent();
		void checkInput();
		void updateAI();
		void updateObjects();
		void checkCollision();
		void updateWindow();
		void mainMenu();
		void restartGame();

		enum EndAction { Nothing, Restart, Menu };
		struct EndButton
		{
			sf::IntRect rect;
			EndAction action;
		};

		EndAction handleClick(sf::Vector2i pos);

		std::vector<EndButton> buttons_;
		sf::Sprite end_;
		sf::Texture texture_;
};

#endif // GAMESTATEEND_H
