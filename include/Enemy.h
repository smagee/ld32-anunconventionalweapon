#ifndef ENEMY_H
#define ENEMY_H

#include <vector>

#include <SFML/Graphics.hpp>

#include "Entity.h"
#include "Bullet.h"

class Enemy : public Entity, public sf::Drawable, public sf::Transformable
{
	public:
		Enemy(sf::Vector2f position);
		virtual ~Enemy();

		void moveBy(sf::Vector2f offset);
		void shoot();

		std::vector<Bullet*> getBullets();
		Bullet* getBullet(int index);
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		std::vector<Bullet*> bullets_;

		sf::Sprite enemy_;
		sf::Texture texture_;
		sf::Texture bulletTex_;
};

#endif // ENEMY_H
