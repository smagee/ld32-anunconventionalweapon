#ifndef GAMESTATEPAUSED_H
#define GAMESTATEPAUSED_H

#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "GameStateGame.h"

class GameStatePaused : public GameState
{
	public:
		GameStatePaused(Game* game, GameStateGame* gameState);
		virtual ~GameStatePaused();
	protected:
	private:
		void checkState();
		void pollEvent();
		void checkInput();
		void updateAI();
		void updateObjects();
		void checkCollision();
		void updateWindow();
		void resumeGame();

		GameStateGame* gameState_;

		sf::View menuView_;

		sf::Sprite pause_;
		sf::Texture texture_;
};

#endif // GAMESTATEPAUSED_H
