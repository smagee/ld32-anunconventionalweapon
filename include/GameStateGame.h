#ifndef GAMESTATEGAME_H
#define GAMESTATEGAME_H

#include <list>

#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "Player.h"
#include "Enemy.h"

class GameStatePaused;

class GameStateGame : public GameState
{
	public:
		GameStateGame(Game* game);
		virtual ~GameStateGame();

		void checkState();
		void pollEvent();
		void checkInput();
		void updateObjects();
		void updateAI();
		void checkCollision();
		void updateWindow();
		void draw();
		void pauseGame();
	protected:
	private:
		sf::View view_;

		sf::Sprite map_;
		sf::Texture texture_;

		Player* player_;
		std::list<Enemy*> enemys_;
};

#endif // GAMESTATEGAME_H
