#include "Enemy.h"

Enemy::Enemy(sf::Vector2f position)
{
	texture_.loadFromFile("assets/sprite_enemy.png");
	enemy_.setTexture(texture_);
	enemy_.setTextureRect(sf::IntRect(0,0,16,16));
	enemy_.setPosition(position);

	movement_ = 10.0f;
	//health_ = 100;
}

Enemy::~Enemy()
{
	//dtor
}

void Enemy::moveBy(sf::Vector2f offset)
{
	enemy_.move(offset);
}

void Enemy::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(enemy_, states);
}

void Enemy::shoot()
{

}

std::vector<Bullet*> Enemy::getBullets()
{
	return bullets_;
}

Bullet* Enemy::getBullet(int index)
{
	return bullets_[index];
}
