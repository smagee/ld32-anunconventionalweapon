#include "GameStateMenu.h"
#include "GameStateGame.h"

GameStateMenu::GameStateMenu(Game* game)
{
	game_ = game;

	texture_.loadFromFile("assets/mainmenu.png");
	menu_.setTexture(texture_);

	MenuButton playButton;
	playButton.action = MenuAction::Play;
	playButton.rect = sf::IntRect(sf::IntRect(250, 250, 300, 100));
	buttons_.push_back(playButton);

	MenuButton exitButton;
	exitButton.action = MenuAction::Exit;
	exitButton.rect = sf::IntRect(sf::IntRect(250, 400, 300, 100));
	buttons_.push_back(exitButton);
}

GameStateMenu::~GameStateMenu()
{

}

void GameStateMenu::checkState()
{

}

void GameStateMenu::pollEvent()
{
	sf::Event event;
	while(game_->window_.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game_->window_.close();
			break;
		case sf::Event::MouseButtonPressed:
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2i pos = sf::Mouse::getPosition(game_->window_);
				MenuAction act = handleClick(pos);
				switch(act)
				{
				case Nothing:
					break;
				case Play:
					startGame();
					break;
				case Exit:
					exitGame();
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}
}

void GameStateMenu::checkInput()
{

}

void GameStateMenu::updateAI()
{

}

void GameStateMenu::updateObjects()
{

}

void GameStateMenu::checkCollision()
{

}

void GameStateMenu::updateWindow()
{
	game_->window_.clear(sf::Color::White);
	game_->window_.draw(menu_);
	game_->window_.display();
}

void GameStateMenu::startGame()
{
	game_->pushState(new GameStateGame(game_));
}

void GameStateMenu::exitGame()
{
	game_->window_.close();
}

GameStateMenu::MenuAction GameStateMenu::handleClick(sf::Vector2i pos)
{
	std::vector<MenuButton>::iterator it;

	for(it = buttons_.begin(); it != buttons_.end(); ++it)
	{
		if((*it).rect.contains(pos))
			return (*it).action;
	}
	return Nothing;
}
