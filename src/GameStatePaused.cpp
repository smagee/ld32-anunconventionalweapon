#include "GameStatePaused.h"

GameStatePaused::GameStatePaused(Game* game, GameStateGame* gameState)
{
	game_ = game;
	gameState_ = gameState;

	texture_.loadFromFile("assets/paused.png");
	pause_.setTexture(texture_);

	sf::Vector2f pos = sf::Vector2f(game_->window_.getSize());
	menuView_.setSize(pos);
	pos *= 0.5f;
	menuView_.setCenter(pos);
}

GameStatePaused::~GameStatePaused()
{

}

void GameStatePaused::checkState()
{

}

void GameStatePaused::pollEvent()
{

}

void GameStatePaused::checkInput()
{
	sf::Event event;
	while(game_->window_.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game_->window_.close();
			break;
		case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Escape)
				resumeGame();
			break;
		default:
			break;
		}
	}
}

void GameStatePaused::updateAI()
{

}

void GameStatePaused::updateObjects()
{

}

void GameStatePaused::checkCollision()
{

}

void GameStatePaused::updateWindow()
{
	game_->window_.clear(sf::Color::White);
	gameState_->draw();
	game_->window_.draw(pause_);
	game_->window_.display();
}

void GameStatePaused::resumeGame()
{
	game_->pushState(gameState_);
}
