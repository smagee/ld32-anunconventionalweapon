#include "GameStateGame.h"
#include "GameStatePaused.h"

GameStateGame::GameStateGame(Game* game)
{
	game_ = game;
	player_ = new Player(sf::Vector2f(400, 300));
	enemys_.push_back(new Enemy(sf::Vector2f(400, 150)));
	enemys_.push_back(new Enemy(sf::Vector2f(250, 300)));
	enemys_.push_back(new Enemy(sf::Vector2f(400, 450)));
	enemys_.push_back(new Enemy(sf::Vector2f(550, 300)));

	texture_.loadFromFile("assets/map.png");
	map_.setTexture(texture_);

	sf::Vector2f pos = sf::Vector2f(game_->window_.getSize());
	view_.setSize(pos);
	pos *= 0.5f;
	view_.setCenter(pos);
}

GameStateGame::~GameStateGame()
{

}

void GameStateGame::checkState()
{

}

void GameStateGame::pollEvent()
{
	checkState();

	sf::Event event;
	while(game_->window_.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game_->window_.close();
			break;
		case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Escape)
				pauseGame();
			break;
		case sf::Event::MouseButtonPressed:
			if (event.key.code == sf::Mouse::Left)
			{
				player_->shoot(player_->getDirection(sf::Mouse::getPosition(game_->window_)));
			}
			break;
		default:
			break;
		}
	}
}

void GameStateGame::checkInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		player_->moveBy(0.0f, -1.0f*player_->getMovementSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		player_->moveBy(0.0f, 1.0f*player_->getMovementSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		player_->moveBy(-1.0f*player_->getMovementSpeed(), 0.0f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		player_->moveBy(1.0f*player_->getMovementSpeed(), 0.0f);
	}
}

void GameStateGame::updateObjects()
{
	std::vector<Bullet*> bullets = player_->getBullets();
	//std::list<Enemy*>::iterator eit;
	//for(eit = enemys_.begin(); eit != enemys_.end(); ++eit)
	//{
		//std::vector<Bullet*> ebullets = (*eit)->getBullets();
		//std::vector<Bullet*>::iterator ebit;
		//for(ebit = ebullets.begin(); ebit != ebullets.end(); ++ebit)
		//{
			//bullets.push_back((*ebit));
		//}
	//}

	std::vector<Bullet*>::iterator it;
	for(it = bullets.begin(); it != bullets.end(); ++it)
	{
		(*it)->move();
	}
}

void GameStateGame::updateAI()
{

}

void GameStateGame::checkCollision()
{

}

void GameStateGame::updateWindow()
{
	game_->window_.setView(view_);
	game_->window_.clear();
	draw();
	game_->window_.display();
}

void GameStateGame::draw()
{
	game_->window_.draw(map_);

	std::list<Enemy*>::iterator eit;
	for(eit = enemys_.begin(); eit != enemys_.end(); ++eit)
	{
		game_->window_.draw(*(*eit));
	}

	game_->window_.draw(*player_);
	std::vector<Bullet*>::iterator it;
	std::vector<Bullet*> bullets = player_->getBullets();
	for(it = bullets.begin(); it != bullets.end(); ++it)
	{
		game_->window_.draw(*(*it));
	}
}

void GameStateGame::pauseGame()
{
	game_->pushState(new GameStatePaused(game_, this));
}
