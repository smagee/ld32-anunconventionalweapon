#include "Bullet.h"

Bullet::Bullet(sf::Sprite bullet, sf::Vector2f direction, sf::Vector2f pos)
{
	bullet_ = bullet;
	direction_ = direction;
	bullet_.setPosition(pos);
}

Bullet::~Bullet()
{
	//dtor
}

void Bullet::move()
{
	bullet_.move(direction_.x * 5.0f, direction_.y * 5.0f);
}

void Bullet::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(bullet_, states);
}
