#include "Game.h"
#include "GameState.h"
#include "GameStateMenu.h"

Game::Game()
{
	window_.create(sf::VideoMode(800, 600), "Ludum Dare 32 - Unconventional Weapon");
	window_.setFramerateLimit(60);
}

Game::~Game()
{
	while(!states_.empty())
		popState();
}

int Game::initGame()
{
	pushState(new GameStateMenu(this));
}

int Game::startGame()
{
	initGame();
	gameLoop();
}

void Game::gameLoop()
{
	while(window_.isOpen())
	{
		if(currentState() == nullptr)
			continue;

		currentState()->checkState();
		currentState()->pollEvent();
		currentState()->checkInput();
		currentState()->updateObjects();
		currentState()->updateAI();
		currentState()->checkCollision();
		currentState()->updateWindow();
	}
}

void Game::pushState(GameState* state)
{
	states_.push(state);
}

void Game::popState()
{
	delete states_.top();
	states_.pop();
}

void Game::changeState(GameState* state)
{
	if(!states_.empty())
		popState();
	pushState(state);
}

GameState* Game::currentState()
{
	if(states_.empty())
		return nullptr;
	return states_.top();
}
