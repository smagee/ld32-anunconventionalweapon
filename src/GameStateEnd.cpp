#include "GameStateEnd.h"

GameStateEnd::GameStateEnd(Game* game)
{
	game_ = game;

	texture_.loadFromFile("assets/end.png");
	end_.setTexture(texture_);

	EndButton restartButton;
	restartButton.action = EndAction::Restart;
	restartButton.rect = sf::IntRect(sf::IntRect(250, 250, 300, 100));
	buttons_.push_back(restartButton);

	EndButton menuButton;
	menuButton.action = EndAction::Menu;
	menuButton.rect = sf::IntRect(sf::IntRect(250, 400, 300, 100));
	buttons_.push_back(menuButton);
}

GameStateEnd::~GameStateEnd()
{
	//dtor
}

void GameStateEnd::checkState()
{

}

void GameStateEnd::pollEvent()
{

}

void GameStateEnd::checkInput()
{
	sf::Event event;
	while(game_->window_.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game_->window_.close();
			break;
		case sf::Event::MouseButtonPressed:
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				sf::Vector2i pos = sf::Mouse::getPosition(game_->window_);
				EndAction act = handleClick(pos);
				switch(act)
				{
				case Nothing:
					break;
				case Menu:
					mainMenu();
					break;
				case Restart:
					restartGame();
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}
}

void GameStateEnd::updateAI()
{

}

void GameStateEnd::updateObjects()
{

}

void GameStateEnd::checkCollision()
{

}

void GameStateEnd::updateWindow()
{

}

void GameStateEnd::mainMenu()
{
	game_->pushState(new GameStateMenu(game_));
}

void GameStateEnd::restartGame()
{
	game_->pushState(new GameStateGame(game_));
}

GameStateEnd::EndAction GameStateEnd::handleClick(sf::Vector2i pos)
{
	std::vector<EndButton>::iterator it;

	for(it = buttons_.begin(); it != buttons_.end(); ++it)
	{
		if((*it).rect.contains(pos))
			return (*it).action;
	}
	return Nothing;
}
