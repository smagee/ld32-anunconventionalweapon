#include "Entity.h"

Entity::Entity()
{
	movement_ = 1.0f;
	health_ = 100.f;
}

Entity::~Entity()
{
	//dtor
}

float Entity::getMovementSpeed()
{
	return movement_;
}

int Entity::getHealth()
{
	return health_;
}
