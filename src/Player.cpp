#include "Player.h"

Player::Player(sf::Vector2f position)
{
	texture_.loadFromFile("assets/sprite_player.png");
	player_.setTexture(texture_);
	player_.setTextureRect(sf::IntRect(0,0,16,16));
	player_.setPosition(position);

	bullet_.setTexture(texture_);
	bullet_.setTextureRect(sf::IntRect(5, 16, 6, 16));

	movement_ = 10.0f;
	health_ = 1;
	//bulletNum_ = 10;
}

Player::~Player()
{
	//dtor
}

void Player::moveBy(int x, int y)
{
	player_.move(x, y);
}

void Player::moveBy(sf::Vector2f offset)
{
	player_.move(offset);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(player_, states);
}

void Player::dodge()
{
	//disable collision
}

void Player::collect()
{
	//bulletNum_ += 1;
}

void Player::shoot(sf::Vector2f direction)
{
	//if(bulletNum_ <= 0)
		//return;
	//bulletNum_ -= 1;
	bullets_.push_back(new Bullet(bullet_, direction, player_.getPosition()));
}

std::vector<Bullet*> Player::getBullets()
{
	return bullets_;
}

Bullet* Player::getBullet(int index)
{
	return bullets_[index];
}

sf::Vector2f Player::getDirection(sf::Vector2i mousePos)
{
	sf::Vector2f myPos = player_.getPosition();
	sf::Vector2f direction = sf::Vector2f(mousePos.x - myPos.x, mousePos.y - myPos.y);
	float hyp = sqrtf(direction.x*direction.x+direction.y*direction.y);
	direction.x /= hyp;
	direction.y /= hyp;
	return direction;
}
